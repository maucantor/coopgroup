# The {coopgroup} repository 

Agent-based models for the evolution of foraging groups
------------------------------------------------
#### Authors
Mauricio Cantor, Damien Farine

#### Maintainer and contact: 
Mauricio Cantor

Departamento de Ecologia e Zoologia, Universidade Federal de Santa Catarina, Brazil

Email: mauriciocantor@yahoo.com 

Telephone: +1 (902) 702-0915


#### VERSION 0.1


We simulate the evolution of foraging groups using two agent-based models: a baseline model and a reproductive model. Their purpose is to investigate (a) the patterns of social structure, particularly resource-use specialisation, that can arise from a simple rule of foraging with previous associates when the previous attempt was successful, and (b) how such interactions can generate structured patterns of relatedness among individuals. These models were built in R 3.2.0 (R Core Team 2015), using functions from igraph (Csardi & Nepusz 2006), sna (Butts 2008), asnipe (Farine 2013), and locfit (Loader 2007) packages.

This repository provides all material to reproduce the results in Cantor & Farine (2018):

1. [the models, their internal functions, and the code to run all analyses](https://bitbucket.org/maucantor/coopgroup/src/bc0f379a52c6aa3cc7efcfde30c08a38d613820a?at=master);

2. [the simulated datasets](https://bitbucket.org/maucantor/coopgroup/src/bc0f379a52c6aa3cc7efcfde30c08a38d613820a/data/?at=master);

3. [the codes to reproduce all figures, including the ones in the supplementary material](https://bitbucket.org/maucantor/coopgroup/src/59ccd6fd9581d9a67a605e914d81d7fc31bdbe14/Figure_codes/?at=master). 



#### REFERENCE
This is a supplementary material of the following article. Please cite the article and refer to this repository:

```
#!r
Cantor M, Farine D. 2018. Simple foraging rules in competitive environments can generate socially-structured populations. Ecology and Evolution.

https://bitbucket.org/maucantor/coopgroup/overview

```